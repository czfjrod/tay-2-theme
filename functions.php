<?php

/*
 * Function: Custom Post Types
 * --------------------------------------
 * tagline - Write in title, list seeds header tagline randomizer
 */
function tay_post_types() {
    register_post_type('tagline', array(
            'labels'                => array(
                'name'                  => _x( 'Taglines', 'Post type general name', 'textdomain' ),
                'singular_name'         => _x( 'Tagline', 'Post type singular name', 'textdomain' ),
                'menu_name'             => _x( 'Taglines', 'Admin Menu text', 'textdomain' ),
                'name_admin_bar'        => _x( 'Tagline', 'Add New on Toolbar', 'textdomain' ),
                'add_new'               => __( 'Add New', 'textdomain' ),
                'add_new_item'          => __( 'Add New Tagline', 'textdomain' ),
                'new_item'              => __( 'New Tagline', 'textdomain' ),
                'edit_item'             => __( 'Edit Tagline', 'textdomain' ),
                'view_item'             => __( 'View Tagline', 'textdomain' ),
                'all_items'             => __( 'All Taglines', 'textdomain' ),
                'search_items'          => __( 'Search Taglines', 'textdomain' ),
                'not_found'             => __( 'No taglines found.', 'textdomain' ),
                'not_found_in_trash'    => __( 'No taglines found in Trash.', 'textdomain' ),
                'archives'              => _x( 'Tagline archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
                'filter_items_list'     => _x( 'Filter taglines list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
                'items_list_navigation' => _x( 'Taglines list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
                'items_list'            => _x( 'Taglines list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
                ),
            'exclude_from_search'   => true,
            'publicly_queryable'    => false,
            'show_ui'               => true,
            'supports'              => array('title')
        ));
    
}
add_action( 'init', 'tay_post_types' );

