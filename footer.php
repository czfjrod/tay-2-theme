        <footer>
            <p class="copyright">&copy; 2019 Talk Amongst Yourselves</p>
            <p class="design"><?php _e('Design by JS*', 'tay'); ?></p>
        </footer>
        <?php wp_footer(); ?>
    </body>
</html>