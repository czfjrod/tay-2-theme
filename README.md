# TALK AMONGST YOURSELVES 2: Electric Bugaloo

This theme repository is meant to plug directly into the web hosting of TAY 2.

## The Why

G/O Media, the latest name in the lineage of TAY and Kotaku's owners after Gawker Media and Gizmodo Media Group, recently got into hot water over an executive editorial stranglehold over Deadspin. With help from the G/O Media Union, the entire staff quit in protest.

Now, G/O Media is seeking to downsize their reach, potentially including reader-run Kinja blogs like Talk Amongst Yourselves.

> Dear Kinja User,
>
> Based on feedback, G/O Media is reevaluating the options for maintaining Kinja user pages and they will remain accessible at this time.
>
> Sincerely,
> G/O Media

## The How

As of the afternoon of November 4, no plans have been made official within the TAY ranks. However, Aikage has reserved the domain name [TAY2.xyz](http://www.tay2.xyz) and called for the TAY Discord users to help come up with ideas to implement for a spinoff website.

This repository is one such idea.

Launched by media generalist J\*Rod, with previous experience in WordPress development and currently building a social web host, this WordPress theme is a basic framework for boundless features not found on Kinja.

## Details

### Custom Post Types
- **Tagline** - Title only, shows up via random query in header

### Custom Taxonomies
- **Column** - Used for sub-blogs and columns like AniTAY, Open Forum, Late TAY Retro

## Contributions

For general suggestions, share them in #townhall on the TAY Discord. For more specific queries, message J*Rod#8473 on Discord. Only approved people can be admitted to contribute to the repository.