<!doctype html>
<html>
    <head>
        <?php wp_head(); ?>
    </head>
    <body>
        <header>
            <h1>
            <span class="title"><?php _e('TALK AMONGST YOURSELVES', 'tay'); ?></span>
                <?php
                    // Retrieve random tagline from database
                    $tagline = new WP_Query(array(
                        'post_type' => 'tagline',
                        'orderby'   => 'rand',
                        'posts_per_page' => 1
                    ));
                ?>
                <?php if( $tagline->have_posts() ) : ?>
                    <?php while ($tagline->have_posts()) : ?>
                        <span class="tag"><?php the_title(); ?></span>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                <?php else: ?>
                    <span class="notag"></span>
                <?php endif; ?>
            </h1>
        </header>